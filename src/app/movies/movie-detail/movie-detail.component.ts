import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Movie } from '../movie.model';
import { MatDialogRef } from '@angular/material';


@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss'],
})

export class MovieDetailComponent implements OnInit, OnDestroy {
  movie = Movie;
  routeSub = null;
  closeResult = '';

  constructor(
      private http: HttpClient, 
      private route: ActivatedRoute, 
      private router: Router,
      public dialogRef: MatDialogRef<MovieDetailComponent>) {
  }
  ngOnDestroy() {
    // on destroy
  }

  ngOnInit() {
    let movieId = this.dialogRef.id;
    this.http.get<any>('https://api.themoviedb.org/3/movie/' + movieId + '?api_key=06dcefc4c6268cb53b82f76560368636').subscribe(data => {
      this.movie = data;
    });
  }

  backClicked() {
    this.router.navigateByUrl('/home');
  }

}
