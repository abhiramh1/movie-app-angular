import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog,MatDialogConfig} from '@angular/material';
import { MovieDetailComponent } from '../movie-detail/movie-detail.component';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MovieSearch} from '../movie.service';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
  animations: [
    trigger('divState', [
      state('in', style({
        opacity: 1,
        transform: 'translateX(0)'
      })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100px)'
        }),
        animate(500)
      ]),
      transition('* => void', [
        animate(500, style({
          transform: 'translateX(100px)',
          opacity: 0
        }))
      ])
    ]),
  ]
})
export class MovieListComponent implements OnInit {
  movies = [];
  movieHomePageList = [];
  favoriteMovies = [];
  closeResult = '';

  constructor(
    private http: HttpClient, 
    private modalService: NgbModal,
    private dialog: MatDialog,
    private router: Router,
    private toastr: ToastrService,
    private myService : MovieSearch
   
    ) { }

  ngOnInit() {
    this.favoriteMovies = JSON.parse(localStorage.getItem('favorites')) ? JSON.parse(localStorage.getItem('favorites')) : [];
    this.http.get<any>('https://api.themoviedb.org/3/trending/movie/week?api_key=06dcefc4c6268cb53b82f76560368636').subscribe(data => {
      this.movies = data.results;
    });

    this.myService.searchedMovies.subscribe(response =>{
      if(response){
        this.movies = response;
      }
    });
  }

  addToFavorites(movieId, originalTitle, voteAverage, voteCount, posterPath) {
    const insertFavData = {
      id: movieId,
      original_title: originalTitle,
      vote_average: voteAverage,
      vote_count: voteCount,
      poster_path: posterPath
    };
    let existingMovieFlag = false;
    for (const element of this.favoriteMovies) {
      if (element.id === movieId) {
        existingMovieFlag = true;
      }
    }
    if (!existingMovieFlag) {
      this.favoriteMovies.push(insertFavData);
      this.toastr.success('Movie added to favorites!');
    } else {
      this.toastr.warning('Movie already added as favorite!');
    }
    localStorage.setItem('favorites', JSON.stringify(this.favoriteMovies));
   
  }

  open(content, movieId) {
    // this.service.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    dialogConfig.id = movieId;
    // let dialogRef = this.dialog.open(MovieDetailComponent, {
    //   height: '400px',
    //   width: '600px',
    // });
    this.dialog.open(MovieDetailComponent, dialogConfig);
    // this.dialog.open(MovieDetailComponent);
    // const modalRef = this.modalService.open(content);
    // modalRef.componentInstance.name = 'World';
    // this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    //   this.closeResult = `Closed with: ${result}`;
    // }, (reason) => {
    //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    // });
  }


  searchMovie(searchText) {    
    this.myService.searchMovieMethod(searchText);    
   }

   goToFavoites() {
    this.router.navigateByUrl('/favorites');
   }
  
}
