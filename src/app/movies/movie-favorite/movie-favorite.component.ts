import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-movie-favorite',
  templateUrl: './movie-favorite.component.html',
  styleUrls: ['./movie-favorite.component.scss'],
  animations: [
    trigger('divState', [
      state('in', style({
        opacity: 1,
        transform: 'translateX(0)'
      })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100px)'
        }),
        animate(500)
      ]),
      transition('* => void', [
        animate(500, style({
          transform: 'translateX(100px)',
          opacity: 0
        }))
      ])
    ]),
  ]
})

export class MovieFavoriteComponent implements OnInit {

  favoriteMovies = [];
  constructor(private router: Router, private toastr: ToastrService) {}

  ngOnInit() {
    this.favoriteMovies = JSON.parse(localStorage.getItem('favorites')) ? JSON.parse(localStorage.getItem('favorites')) : [];
  }

  removeFromFavorites(movieId) {
    for (let i = 0; i < this.favoriteMovies.length; i++) {
      if (this.favoriteMovies[i].id === movieId) {
        this.favoriteMovies.splice(i, 1);
      }
    }
    this.toastr.success('Movie removed from favorites!');
    localStorage.setItem('favorites', JSON.stringify(this.favoriteMovies));
  }

  backClicked() {
    this.router.navigateByUrl('/home');
  }

}
