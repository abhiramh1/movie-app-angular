import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Movie } from './movie.model';
import { BehaviorSubject } from 'rxjs';



@Injectable()
export class MovieSearch {
    movies = [];
    constructor(
        private http: HttpClient
    ) { }
        public searchedMovies =  new BehaviorSubject<any>(null);
    searchMovieMethod(searchText) {
        if (searchText === '') {
            this.http.get<any>('https://api.themoviedb.org/3/trending/movie/week?api_key=06dcefc4c6268cb53b82f76560368636').subscribe(data => {
              this.searchedMovies.next(data.results);
            });
        } else {
            this.http.get<any>('https://api.themoviedb.org/3/search/movie?api_key=06dcefc4c6268cb53b82f76560368636&language=en-US&query='
                + searchText + '&page=1&include_adult=false').subscribe(data => {
                this.searchedMovies.next(data.results);
                });
        }
        
    }
}
