export class Movie {
	public id: string;
	public title: string;
	public overview: string;
	public genres: [];
	public posterPath: string;

	constructor(id: string, key: string, name: string, overview: string, genres: [], rate: string, length: string,
		posterPath: string) {
		this.id = id;
		this.title = this.title;
		this.genres = genres;
		this.overview = overview;
		this.posterPath = posterPath;
	}
}

