import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { MovieListComponent } from './movies/movie-list/movie-list.component';
import { MovieDetailComponent } from './movies/movie-detail/movie-detail.component';
import { MovieFavoriteComponent } from './movies/movie-favorite/movie-favorite.component';

const appRoutes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full'},
    { path: 'home',  component: MovieListComponent },
    { path: 'movie/:uri',  component: MovieDetailComponent },
    { path: 'favorites',  component: MovieFavoriteComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
